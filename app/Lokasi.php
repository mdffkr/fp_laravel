<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    //
    public $timestamps = false;
    protected $table = "lokasi";
    protected $fillable = ["kota", "provinsi"];

    public function event()
    {
        return $this->hasMany('App\Event');
    }
}
