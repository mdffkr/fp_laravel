<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    public $timestamps = false;
    protected $table = "event";
    protected $fillable = ["nama", "waktu","kategori_id","organizer_id","lokasi","lokasi_id",'harga','deskripsi'];

    public function lokasi()
    {
        return $this->belongsTo('App\Lokasi');
    }
    public function organizer()
    {
        return $this->belongsTo('App\Organizer');
    }
    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }
}
