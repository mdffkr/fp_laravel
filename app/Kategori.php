<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    //
    public $timestamps = false;
    protected $table = "Kategori";
    protected $fillable = ["nama", "event_id"];

    public function event()
    {
        return $this->hasMany('App\Event');
    }
}
