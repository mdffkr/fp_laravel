<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Kategori;
use App\Komentar;
use App\Organizer;
use App\Profile;
use App\Lokasi;
use App\User;
use Auth;
use DB;
class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('index','show');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $event = Event::all();
        $lokasi = Lokasi::all();

        return view('event.index', compact('event','lokasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $iduser= Auth::user()->id;
        // dd($idprof);
        $idprof= Profile::select('id')->where("user_id",$iduser)->value('id');
        $idorg = Organizer::where('profile_id',$idprof)->first();
        $lokasi= DB::table('lokasi')->get();
        // dd($idorg);
    
        $kategori = DB::table('kategori')->get();
        
        
        return view('event.create',compact("lokasi","idorg","kategori"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
    		'nama' => 'required',
    		'waktu' => 'required',
            'lokasi' =>  'required',
            'kategori_id' => 'required',
            'lokasi_id' => 'required',
            'organizer_id' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required',
    	]);
 
        Event::create([
    		'nama' => $request->nama,
    		'waktu' => $request->waktu,
            'lokasi' => $request->lokasi,
            'kategori_id' => $request->kategori_id,
            'lokasi_id' => $request->lokasi_id,
            'organizer_id' => $request->organizer_id,
            'deskripsi' => $request->deskripsi,
            'harga' => $request->harga,
    	]);
        
    	return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $event = Event::find($id);
        $idlokasi= $event->value('lokasi_id');
        $idkategori= $event->value('kategori_id');
        $idorg= $event->value('organizer_id');
        
        $iduser= Auth::user()->id;
        $user= User::all();
        // dd($idprof);
        // $idprof= Profile::select('id')->where("user_id",$iduser)->value('id');
        
        $lokasi= Lokasi::where('id',$idlokasi)->first();
        // dd($idorg);
        $org = Organizer::where('id',$idorg)->first();
        $kategori = Kategori::where('id',$idkategori)->first();
        // dd($idkategori);
        $hitungkomentar = Komentar::where('event_id',$id)->count();
        $komentar = Komentar::where('event_id',$id)->get();
        // dd($komentar);
        
        return view('Event.show', compact('event','kategori','lokasi','org','komentar','user'));
        
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $event = Event::find($id);
        $iduser= Auth::user()->id;
        // dd($idprof);
        $idprof= Profile::select('id')->where("user_id",$iduser)->value('id');
        $idorg = Organizer::where('profile_id',$idprof)->first();
        $lokasi= DB::table('lokasi')->get();
        // dd($idorg);
    
        $kategori = DB::table('kategori')->get();
        return view('event.edit',compact("lokasi","idorg","kategori","event"));

      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
    		'nama' => 'required',
    		'waktu' => 'required',
            'lokasi' =>  'required',
            'kategori_id' => 'required',
            'lokasi_id' => 'required',
            'organizer_id' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required',
    	]);
        $event = Event::findorfail($id);
        $data=[
            'nama' => $request->nama,
    		'waktu' => $request->waktu,
            'lokasi' => $request->lokasi,
            'kategori_id' => $request->kategori_id,
            'lokasi_id' => $request->lokasi_id,
            'organizer_id' => $request->organizer_id,
            'deskripsi' => $request->deskripsi,
            'harga' => $request->harga,
        ];
        $event->update($data);
        return redirect('/Event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $event = Event::findorfail($id);
        $event->delete();
    
        return redirect('/Event');
    }
}
