@extends('listco.master')

@section('content')
<div class="slider-area hero-bg2 hero-overly">
        <div class="single-slider hero-overly  slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10 col-lg-10">
                        <!-- Hero Caption -->
                        <div class="hero__caption hero__caption2 pt-200">
                            <h1>Explore what you are finding</h1>
                        </div>
                        <!--Hero form -->
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
   


  <!-- listing Area Start -->
  <div class="listing-area pt-120 pb-120">
    <div class="container">
                <!--? Popular Directory Start -->
                <div class="popular-directorya-area fix">
                
                        <div class="row">
                            @forelse($event as $item)
                            <div class="col-lg-6">
                                <!-- Single -->
                                <div class="properties properties2 mb-30">
                                    <div class="properties__card">
                                        
                                        <div class="properties__caption">
                                            <h3><a href='Event/{{$item->id}}'>{{$item->nama}}</a></h3>
                                            <p>{{$item->deskripsi}}</p>
                                        </div>
                                        <div class="properties__footer d-flex justify-content-between align-items-center">
                                            <div class="restaurant-name">
                                                <img src="assets/img/gallery/restaurant-icon.png" alt="">
                                                <h3>Rp{{$item->harga}}</h3><br>
                                                <a class="btn btn-primary my-2" href="Event/{{$item->id}}/edit" role="button">Edit</a>
                                                <form action="/Event/{{$item->id}}" method='post'>
                                                    @csrf
                                                    @method("DELETE")
                                                    <input type="submit" value="delete" class="btn btn-danger my-2">
                                                </form>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            @empty
                            <h1 class="text-center">There is no data</h1>
                            @endforelse
                        </div>
                        
                </div>
                <!--? Popular Directory End -->
                <!--Pagination Start  -->
                
            </div>
        </div>
@endsection