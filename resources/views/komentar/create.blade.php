<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Add Event</title>

    <!-- Icons font CSS-->
    <link href="{{asset('colorlib/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('colorlib/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="{{asset('colorlib/vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('colorlib/vendor/datepicker/daterangepicker.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('colorlib/css/main.css')}}" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h2 class="title">Comment Form</h2>
                    <form method="POST" action="{{ route('Komentar.store') }}" enctype="multipart/form-data">
                        @csrf
                        
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Nama</label>
                                    <input class="input--style-4" readonly="readonly" value="{{Auth::user()->nama}}" type="text" >
                                </div>
                            </div>
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Komentar</label>
                                    <input class="input--style-4" type="text" name="komentar" >
                                </div>
                            </div>
                        </div>
                            @error('waktu')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                            <input class="input--style-4" readonly="readonly" value="{{Auth::user()->id}}" type="text" name="nama">
                            
                          
                        
                        <br>
                        <div class="p-t-15">
                            <button class="btn btn--radius-2 btn--blue" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
        
    <!-- Jquery JS-->
    <script src="{{asset('colorlib/vendor/jquery/jquery.min.js')}}"></script> 
    <script src="{{asset('colorlib/vendor/jquery/jquery.min.js')}}"></script>
    <!-- Vendor JS-->
    <script src="{{asset('colorlib/vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('colorlib/vendor/datepicker/moment.min.js')}}"></script>
    <script src="{{asset('colorlib/vendor/datepicker/daterangepicker.js')}}"></script>

    <!-- Main JS-->
    <script src="{{asset('colorlib/js/global.js')}}"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->